# encoding: utf-8

import glob
import os.path as osp
import re
import warnings

from .bases import ImageDataset
from ..datasets import DATASET_REGISTRY


@DATASET_REGISTRY.register()
class Real28(ImageDataset):
    """Real28.

    Dataset statistics:
        - identities:  .
        - images:  (train) +  (query) +  (gallery).
    """
    dataset_dir = ''
    dataset_name = "real28"

    def __init__(self, root='datasets', **kwargs):
        # self.root = osp.abspath(osp.expanduser(root))
        self.root = root
        self.dataset_dir = osp.join(self.root, self.dataset_dir)

        # allow alternative directory structure
        self.data_dir = self.dataset_dir
        data_dir = osp.join(self.data_dir, 'Real28')
        if osp.isdir(data_dir):
            self.data_dir = data_dir
        else:
            warnings.warn('The current data structure is deprecated. Please '
                          'put data folders such as "train" under '
                          '"Real28".')

        self.query_dir = osp.join(self.data_dir, 'query')
        self.gallery_dir = osp.join(self.data_dir, 'gallery')

        required_files = [
            self.data_dir,
            self.query_dir,
            self.gallery_dir,
        ]
        self.check_before_run(required_files)

        query = self.process_dir(self.query_dir, is_train=False)
        gallery = self.process_dir(self.gallery_dir, is_train=False)

        super(Real28, self).__init__([], query, gallery, **kwargs)

    def process_dir(self, dir_path, is_train=True):
        img_paths = glob.glob(osp.join(dir_path, '*.jpeg'))

        data = []
        for img_path in img_paths:
            img_name = img_path.split('/')[-1]
            print([img_name[:2], img_name[3:5]])
            pid, camid = map(int, [img_name[:2], img_name[3:5]])
            assert 1 <= pid <= 28  # pid == 0 means background
            assert 1 <= camid <= 4
            camid -= 1  # index starts from 0
            if is_train:
                pid = self.dataset_name + "_" + str(pid)
            data.append((img_path, pid, camid))

        print(data)
        return data
